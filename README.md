# Build an MSP with Open Source Software
This repository is built to support the series I'm making on my YouTube channel https://youtube.com/c/AwesomeOpenSource. The whole idea is to build up an MSP business (Managed Service Provider) using open source software, and as your business grows giving back to the projects you use in order to ensure their longevity.  Your success becomes their success.

## Getting started
You will want to check out the various articles I create for the series on my Bookstack site:
- https://wiki.opensourceisawesome.com/books/build-an-msp-on-open-source

There are some spreadsheets I'll be adding here as well. They are built using LibreOffice Calc.
1. A simple pricing spreadsheet. 

This will allow you to enter Pricing per Month for various parts of a business you'd likely support as an MSP. Next you can enter the number of these items, and finally enter various discounts as needed.  I'll iterate on this, and invite anyone else to also iterate and make this a more professional tool that we can use. 

2. A simple time reporting spreadsheet - hopefully replaced with something better over time.

This allows you to report the time you spend working on your clients systems, and calculate that using a cost comparison to what you charge per momnth, vs. what it would cost the client if they were to pay you by the hour for the same tasks.  Generally, their monthly cost paid to you should be less than what they would be paying by the hour for your services.

I hope you'll enjoy the series, and stick with me as we move forward on this journey.